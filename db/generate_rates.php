<?php

$filename = 'alior.json';
$outputfilename = 'rates-data.sql';
$exchname = 'ALI';

$fh = fopen($filename,'r');
$fh2 = fopen($filename,'r');

$line = 1;

$y = json_decode(fgets($fh2))->currencies;

$write = fopen($outputfilename,'w');

$str = 'INSERT INTO currency_exchangers(id,exchanger_name) VALUES(\'' . $exchname . '\',\'Alior\');' . "\n";

$str .= 'INSERT INTO financial_instruments(id,currency_1_id,currency_2_id,exchanger_id) VALUES'."\n";

$crncs = array();

foreach($y as $yy) {
	$crncs[] = $yy->currency1;
	$crncs[] = $yy->currency2;
	$str .= '(\'' . $exchname . $yy->currency1 . $yy->currency2 . '\',\'' . $yy->currency1 . '\',\''	. $yy->currency2 . '\',\'' . $exchname . '\'),' . "\n";
	$str .= '(\'' . $exchname . $yy->currency2 . $yy->currency1 . '\',\'' . $yy->currency2 . '\',\''	. $yy->currency1 . '\',\'' . $exchname . '\'),' . "\n";
}

$crncs = array_unique($crncs);
$str = substr_replace($str,";\n",-2);
$str2 = "\nINSERT INTO currencies(id,currency_name) VALUES\n";
foreach($crncs as $x) {
		$str2 .= '(\''.$x . '\',\'' . $x . '\'),' . "\n";
}


$str2 = substr_replace($str2,";\n",-2);
$str = $str2 . $str;
fwrite($write,$str);


while(!feof($fh2)) { fgets($fh2); $line++;}
fclose($fh2);

echo "0%\n";

$header = 'INSERT INTO rates(instrument_id,timestamp,value) VALUES ';

$ll = 0;
$prev = 0;
$cnt = 0;
$packages = 0;
$txt = array();
while(!feof($fh)) {
		$ll++;
		$x = (int)(10*$ll/$line);
		if($x != $prev) {
				$prev = $x;
				echo $prev . "0%\n";
		}
		$l = fgets($fh);
		if(!$l) continue;
		$lx = json_decode($l);
		$timestamp = strtotime($lx->lastUpdate);
		$l = $lx->currencies;
		foreach($l as $x) {
			if($cnt > 100000) {
				fwrite($write, $header . implode(',',$txt) . ';' . "\n");
				$txt = array();
				$cnt=0;
				$packages++;
			}
			
			$sell = (double)str_replace(',','.',$x->sell);
			$buy = 1.0/(double)str_replace(',','.',$x->buy);
			
			$txt[] = '(\'' . $exchname . $x->currency1 . $x->currency2 . '\','  . $timestamp . ',' . $sell . ')';
			$txt[] = '(\'' . $exchname . $x->currency2 . $x->currency1 . '\','  . $timestamp . ',' . $buy . ')';
			$cnt++;
		}
}
fwrite($write,$header . implode(',',$txt). ';' . "\n" );
fclose($write);
fclose($fh);

