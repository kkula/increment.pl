DROP TABLE IF EXISTS currency_exchangers CASCADE;
DROP TABLE IF EXISTS currencies CASCADE;
DROP TABLE IF EXISTS financial_instruments CASCADE;
DROP TABLE IF EXISTS rates CASCADE;
DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS graph_schemes CASCADE;
DROP TABLE IF EXISTS strategies CASCADE;
DROP TABLE IF EXISTS user_strategies CASCADE;
DROP TABLE IF EXISTS graphs CASCADE;
DROP TABLE IF EXISTS edges CASCADE;
DROP TABLE IF EXISTS simulations CASCADE;
DROP TABLE IF EXISTS transactions CASCADE;
DROP TABLE IF EXISTS queue CASCADE;

CREATE TABLE currency_exchangers (
	id TEXT NOT NULL PRIMARY KEY,
	exchanger_name TEXT NOT NULL
);

CREATE TABLE currencies (
	id TEXT NOT NULL PRIMARY KEY,
	currency_name TEXT NOT NULL
);

CREATE TABLE financial_instruments (
	id TEXT NOT NULL PRIMARY KEY,
	currency_1_id TEXT REFERENCES currencies,
	currency_2_id TEXT REFERENCES currencies,
	exchanger_id TEXT REFERENCES currency_exchangers
);

CREATE TABLE rates (
	instrument_id TEXT REFERENCES financial_instruments,
	"timestamp" INTEGER NOT NULL,
	"value" NUMERIC NOT NULL,
	PRIMARY KEY (instrument_id, "timestamp")
);

CREATE INDEX ON rates (instrument_id);
CREATE INDEX ON rates ("timestamp");

CREATE TABLE users (
	id SERIAL PRIMARY KEY,
	mail TEXT NOT NULL,
	"name" TEXT NOT NULL,
	surname TEXT NOT NULL,
	password TEXT NOT NULL,
	UNIQUE(mail)
);

CREATE TABLE graph_schemes (
  id SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  available BOOLEAN NOT NULL DEFAULT FALSE,
  UNIQUE("name")
);

CREATE TABLE strategies (
 id SERIAL NOT NULL PRIMARY KEY,
 "name" TEXT NOT NULL,
 available BOOLEAN NOT NULL DEFAULT FALSE,
 UNIQUE ("name")
);

CREATE TABLE user_strategies (
 id SERIAL NOT NULL PRIMARY KEY,
 user_id INTEGER NOT NULL REFERENCES users,
 strategy_id INTEGER NOT NULL REFERENCES strategies,
 parameters TEXT NOT NULL,
 memory TEXT,
 name TEXT
);

CREATE TABLE graphs (
 id SERIAL NOT NULL PRIMARY KEY,
 user_id INTEGER NOT NULL REFERENCES users,
 "name" TEXT,
 exchanger_id TEXT NOT NULL REFERENCES currency_exchangers
);

CREATE TABLE edges (
 id SERIAL NOT NULL PRIMARY KEY,
 instrument_id TEXT NOT NULL REFERENCES financial_instruments,
 graph_id INTEGER NOT NULL REFERENCES graphs,
 user_strategy_id INTEGER NOT NULL REFERENCES user_strategies
);

CREATE TABLE simulations (
 id SERIAL NOT NULL PRIMARY KEY,
 "name" TEXT NOT NULL,
 graph_id INTEGER NOT NULL REFERENCES graphs,
 online BOOLEAN NOT NULL DEFAULT FALSE,
 initial_currency TEXT NOT NULL REFERENCES currencies,
 initial_amount NUMERIC NOT NULL CHECK (initial_amount > 0),
 start_date INTEGER NOT NULL,
 end_date INTEGER,
 status INTEGER
);

CREATE TABLE queue (
	id SERIAL NOT NULL PRIMARY KEY,
	simulation_id INTEGER NOT NULL REFERENCES simulations ON DELETE CASCADE,
	"timestamp" INTEGER,
	principal TEXT NOT NULL
);


CREATE TABLE transactions (
	id SERIAL NOT NULL PRIMARY KEY,
	simulation_id INTEGER NOT NULL REFERENCES simulations ON DELETE CASCADE,
	"timestamp" INTEGER NOT NULL,
	swap_rate NUMERIC NOT NULL,
	swap_message TEXT NOT NULL,
	edge_used INTEGER NOT NULL REFERENCES edges,
	final_currency TEXT NOT NULL REFERENCES currencies,
	final_amount NUMERIC NOT NULL
);
