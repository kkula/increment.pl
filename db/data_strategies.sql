INSERT INTO graph_schemes ("name", available) VALUES ('LimitScheme', true), ('MovingAverageScheme', true), ('ROCScheme', true);


INSERT INTO strategies ("name", available) VALUES ('Limit', true), ('Average', true), ('ImprovedAverage', true), ('ROC', true);
