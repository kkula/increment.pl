DROP VIEW IF EXISTS user_simulations CASCADE;
DROP VIEW IF EXISTS user_strategies_v CASCADE;

CREATE VIEW user_simulations
	(user_id, simulation_name)
AS (SELECT u.id, s.name
	FROM users u
	JOIN graphs g
	ON (u.id = g.user_id)
	JOIN simulations s
	ON (g.id = s.graph_id));

CREATE VIEW user_strategies_v AS
SELECT e.id as edge_id, us.memory AS memory, s.id as simulation_id,currency_1_id AS start_currency,currency_2_id AS end_currency,instrument_id,
us.parameters,str.name AS strategy_name,us.id AS us_id
 FROM edges AS e JOIN simulations AS s ON e.graph_id=s.graph_id
JOIN financial_instruments AS i ON i.id=e.instrument_id
JOIN user_strategies AS us ON us.id=user_strategy_id
JOIN strategies AS str ON str.id=us.strategy_id;
