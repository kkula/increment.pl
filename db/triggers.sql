

CREATE FUNCTION trigger_insert() RETURNS trigger AS $$
BEGIN

	IF NEW.value = (SELECT value FROM rates WHERE rates.instrument_id = NEW.instrument_id AND timestamp<NEW.timestamp ORDER BY timestamp DESC LIMIT 1) THEN
		RAISE EXCEPTION 'Repeatable value. Ommiting';
	END IF;
	RETURN NEW;
	

END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER insert_raise BEFORE INSERT ON rates
FOR EACH ROW EXECUTE PROCEDURE trigger_insert();
