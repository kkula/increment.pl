/*
Input format:
CHF/JPY,20130401 00:00:00.025,99.201,99.226
CHF/JPY,20130401 00:00:00.185,99.203,99.227
CHF/JPY,20130401 00:00:00.325,99.204,99.228
CHF/JPY,20130401 00:00:00.622,99.205,99.23
*/


#include <cstdio>
#include <cstdlib>
#include <ctime>

using namespace std;



const long long MIN_SEC_DIST = 10;		//Minimalny odstęp między kolejnymi przepisanymi rekordami



int main()
{
	char wal1[10]={}, wal2[10]={};
	double buy, sell, sec;
	time_t tmp, epoch;
	tm D;
	long long int timestamp = -1, lastPrinted = -1;
	
	
	time (&tmp);
	D = *gmtime(&tmp);
	
	D.tm_sec = 0;
	
	D.tm_min = 0;
	D.tm_hour = 0;
	D.tm_mday = 1;
	D.tm_mon = 0;
	D.tm_year = 70;
	D.tm_wday = 4;
	D.tm_yday = 0;
	
	epoch = mktime(&D);
	
// 	printf("daytime saving mode: %d\n", epoch.tm_isdst);
	
	//D.tm_year+=43;
	//printf("Timestamp: %lf\n\n", difftime(mktime(&D), epoch));
	printf ("INSERT INTO rates (\"instrument_id\", \"timestamp\", \"value\") VALUES\n");
	
	
	while (scanf("%3[^/]/%3[^,],%4d%2d%2d %2d:%2d:%lf,%lf,%lf ", wal1, wal2, &D.tm_year, &D.tm_mon, &D.tm_mday, &D.tm_hour, &D.tm_min, &sec, &buy, &sell) != EOF)
	{
		D.tm_sec = (int)(sec);
		//printf("Wal1: %s\nWal2: %s\nYear: %d\nMonth: %d\nDay: %d\nHour: %d\nMinute: %d\nSec: %d\nBuy: %lf\nSell: %lf\n", wal1, wal2, D.tm_year, D.tm_mon, D.tm_mday, D.tm_hour, D.tm_min, D.tm_sec, buy, sell);
		
		D.tm_mon-=1;
		D.tm_year-=1900;
		
		timestamp = (long long int)(difftime(mktime(&D), epoch));
		
		if (timestamp-lastPrinted<MIN_SEC_DIST)
			continue;
		
		if (lastPrinted != -1)
			printf(",\n");
		
		lastPrinted = timestamp;
		
		//printf("Timestamp: %Ld\n\n", timestamp);
		
		//printf("(\'%s%s\',\'%s\',\'%s\',\'TRUEFX\')\n", wal1, wal2, wal1, wal2);
		printf("(\'TRU%s%s\',\'%Ld\',\'%lf\'),\n", wal1, wal2, timestamp, buy);
		printf("(\'TRU%s%s\',\'%Ld\',\'%lf\')", wal2, wal1, timestamp, 1.0/sell);
	}
	
	printf(";\n");
	
	return 0;
}
