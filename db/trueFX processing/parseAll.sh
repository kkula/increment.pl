#!/bin/bash


g++ -Wall -O2 parse.cpp -o parse.e


time (
for i in * 
do
	if !(test -f "$i")
	then
		cd "$i"
		
		for j in *.csv 
		do
			if test -f "$j"
			then
				echo "$j"
				time (./../parse.e < "$j" > "$j.parsed")
			fi
		done
		
		cd ".."
	fi
done
)