#!/bin/bash

for i in * 
do
	if !(test -f "$i")
	then
		cd "$i"
		
		for j in * 
		do
			if test -f "$j"
			then
				unzip "$j"
			fi
		done
		
		cd ".."
	fi
done