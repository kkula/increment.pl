#!/bin/bash



time (
for i in * 
do
	if !(test -f "$i")
	then
		cd "$i"
		
		for j in *.csv.parsed 
		do
			if test -f "$j"
			then
				echo "$j"
				cat "$j" >> ../merged_rates.sql
			fi
		done
		
		cd ".."
	fi
done
)