<?php

namespace Increment\Simulator;

class BasicSimulator extends Simulator {
	
	const SELL_LIMIT = 0.00;

	protected function _simulate($timestamp, $strategies) {
		
		if(!count($strategies)) {
			return array(
				'status' => self::CURRENCY_HOLD
			);
		}
		
		$max = -10.0; // -INF for (-1,1) range
		
		$results = array();
		
		foreach($strategies as &$strategy) {
			
			$max = max(array(
				$results[] = $strategy->simulate($timestamp),
				$max
			));
		}
		
		if($max < self::SELL_LIMIT) {
			return array(
				'status' => self::CURRENCY_HOLD
			);
		} else {
			$maxId = array_search($max, $results);
			
			return array(
				'status' => self::CURRENCY_SELL,
				'strategy' => $strategies[$maxId]
			);
		}
		
	}
}