<?php

namespace Increment\Simulator;

abstract class Simulator {
	
	const CURRENCY_HOLD = 0;
	const CURRENCY_SELL = 1;

	/**
	 *
	 * @var \Increment\Db\Simulation
	 */
	protected $db = NULL;
	
	protected $simulation_id = NULL;
	
	protected $save_results = TRUE;
	
	protected $current_currency = NULL;
	
	protected $current_amount = NULL;
	
	protected $str_cache = array();
	
	public function __construct($simulation_id, $save_results = TRUE) {
		$this->db = new \Increment\Db\Simulation();
		$this->simulation_id = $simulation_id;
		$this->save_results = $save_results;
		$this->current_currency = $this->db->getCurrentCurrency($this->simulation_id);
		$this->current_amount = $this->db->getCurrentAmount($this->simulation_id);
	}
	
	public function getCurrentCurrency() {
		return $this->current_currency;
	}
	
	public function makeStep($timestamp,$saveMemory = false) {
		
		$result = $this->_simulate($timestamp, $this->_getStrategies($this->current_currency));
		if($result['status'] == self::CURRENCY_HOLD) {
			// Do nothing. We should hold current currency
		} else {
			// exchange
			
			$model = $result['strategy']->getModel();
			$this->current_currency = $model->getResultCurrency();
			$rate = $model->getValue($timestamp);
			if(!$rate) {
				return;
			}
			$this->current_amount   = $this->current_amount / $rate;
			$message = $result['strategy']->getMessage();
			
			if($this->save_results) {
				$this->_saveResult(array(
					'currency' => $this->current_currency,
					'amount' => $this->current_amount,
					'message' => $message,
					'rate' => $rate,
					'timestamp' => $timestamp,
					'edge_id' => $result['strategy']->getEdgeId()
				));
			}
			
			// we have to save all memory here.
			foreach($this->_getStrategies($this->current_currency) as $class) {
				$this->_memsave($class->getId(), $class->getMemory());
			}
		}
		
		// force saving memory on each step.
		if($saveMemory) {
			foreach($this->_getStrategies($this->current_currency) as $class) {
				$this->_memsave($class->getId(), $class->getMemory());
			}
		}
	}
	
	protected function _memsave($id,$mem) {
		$memsave = new \Increment\Db\Simulation();
		$memsave->saveMemory($id,json_encode($mem));
	}
	
	public function simulate() {
		$timestamps = $this->db->getSimulationTimes($this->simulation_id);
		$tick = $timestamps['start_date'];
		$end = $timestamps['end_date'];
    
		do {
		
			$this->makeStep($tick,false);
			$dbCurrency = new \Increment\Dataset\Currency($this->getCurrentCurrency());
			$tick = $dbCurrency->getNextTick($tick);
			
		} while($tick && ($tick < $end || $end === null));
		
		foreach($this->_getStrategies($this->current_currency) as $class) {
				$this->_memsave($class->getId(), $class->getMemory());
		}
		
		
	}
	
	protected function _getStrategies($currency) {
		
		if(isset($this->str_cache[$currency]))
			return $this->str_cache[$currency];
		else {
			$this->str_cache = array(); // unseting strategy cache - to allow other edges modify memory.
		}
		
		$strategies = $this->db->getStrategies($this->simulation_id, $currency);
		$classes = array();
		foreach($strategies as $str) {
			$model = new \Increment\Dataset\Currency($str['instrument_id']);
			$classname = '\\Increment\\Strategy\\' . $str['strategy_name'] . 'Strategy';
			$classes[] = new $classname($model,json_decode($str['parameters']),$str['edge_id'],json_decode($str['memory']),$str['us_id']);
		}
		
		$this->str_cache[$currency] = $classes;
		
		return $classes;
	}
	
	private function _saveResult($config) {
		$this->db->saveTransation($this->simulation_id, $config);
	}
	
	abstract protected function _simulate($timestamp, $strategies);
}