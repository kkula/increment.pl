<?php

namespace Increment\Strategy;

class LimitStrategy extends Strategy {

	protected function _simulate($timestamp) {
		
		$current = $this->db->getValue($timestamp);
		$limit = $this->parameters->limit;
		
		if($current < $limit) {
			return array(
				'result' => self::RESULT_MAX,
				'message' => 'Sell impuls: ' . $current . ' < ' . $limit
			);
		}
		
		return array(
			'result' => self::RESULT_MIN,
			'message' => 'Nothing to do.'
		);
	}
  
}
?>
