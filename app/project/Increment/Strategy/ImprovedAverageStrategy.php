<?php

namespace Increment\Strategy;

class AverageStrategy extends Strategy {

	protected function _simulate($timestamp) {
		
    $current = $this->db->getValue($timestamp);
		
		$res = $this->db->getFilteredAverage(
            $timestamp,
            $this->parameters->window_length,
            $current,
            $this->parameters->discard_perc);
		
    $avg = $res['avg'];
		
    if($res['discarded'] < $this->parameters->discard_length
            && $avg != 0 && $current / $avg <= (100 - $this->parameters->perc) / 100.0 ) {
      
      /* save rate for reversed transaction */
      $sim_db = new \Increment\Db\Simulation();
      $reversedId = $sim_db->getReverseUserStrategyId($this->id);
      $sim_db->saveMemory($reversedId, json_encode(1 / $current));
      
			return array(
				'result' => self::RESULT_MAX,
				'message' => 'Sell impuls: ' . $current . '/ ' . (string)$avg . ' ' . ' < ' . $this->parameters->perc
			);
		}
		
		return array(
			'result' => self::RESULT_MIN,
			'message' => 'Nothing to do.'
		);
	}
  
}
?>
