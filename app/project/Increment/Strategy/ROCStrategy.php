<?php

namespace Increment\Strategy;


class ROCStrategy extends Strategy {

	protected function _simulate($timestamp) {
		
		//Passed parameters:
		$n = $this->parameters->n; //number of days
		$m = $this->parameters->m; //number od seconds
		$accuracy = 5;
		$time = $timestamp - $m;
		$sum = 0; //I count average from sum and howMany
		$howMany =0;
		if ((count($this->memory) > 1) && ($this->memory[0] != null) && ($this->memory[1] != null)) { //I check whether I have some previous data
			$howMany = $this->memory[1];
			$sum = $this->memory[0];
			$lastValueBefore = $this->db->getValue($time-60*60*24*$n-$m);
			$lastCurrent = $this->db->getValue($time-$m);
			if ($lastValueBefore != null && $lastCurrent != null) { 
				$sum = $sum + $lastValueBefore - $lastCurrent;
				$howMany = $howMany - 1;
			}
			$current = $this->db->getValue($timestamp); //I count ROC from current and valueBefore and then count average from it
			$valueBefore = $this->db->getValue($timestamp-60*60*24*$n);
			if ($valueBefore != null && $current != null) {
				$howMany = $howMany + 1;
				$sum = $sum + $current - $valueBefore;
			}
		}
		else { //I count the whole average
			 while ($time < $timestamp) {
				$current = $this->db->getValue($time); //I count ROC from current and valueBefore and then count average from it
				$valueBefore = $this->db->getValue($time-60*60*24*$n);
				if ($valueBefore != null && $current != null) {
					$howMany = $howMany + 1;
					$sum = $sum + $current - $valueBefore;
				}
				$time = $time + $accuracy;		
			}
		}
		if ($howMany > 0) { //If there was some previous data I consider selling
			$avg = ($sum- $this->db->getValue($timestamp) + $this->db->getValue($timestamp-60*60*24*$n))/$howMany;
			$lastAvg = $this->memory[0]/$this->memory[1];
			if ($avg * $lastAvg < 0) { 
				if ($avg != 0) { //I need to remember whether I was previously above or beneath the plot so 
								//I ignore my result if it equals 0.
					$this->memory[0] = $sum;
					$this->memory[1] = $howMany;
				}
				
				return array(
					'result' => self::RESULT_MAX,
					'message' => 'Sell.'
				);	
			}
		}
		if ($sum != 0 && $howMany != 0) {
				$this->memory[0] = $sum;
				$this->memory[1] = $howMany;
		}
		return array(
			'result' => self::RESULT_MIN,
			'message' => 'Do not sell.'
		);
	}
  
}
?>
