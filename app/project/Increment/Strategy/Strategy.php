<?php

namespace Increment\Strategy;

abstract class Strategy {
	
	const RESULT_MAX = +1.0;
	const RESULT_MIN = -1.0;
	const DEFAULT_MESSAGE = 'No message.';
	
	private $message = NULL;
	protected $db = NULL;
	protected $parameters = array();
	protected $memory = array();
	protected $edge_id = NULL;
	protected $id = NULL;


	public function __construct($model,$parameters,$edge_id = NULL,$memory = NULL,$id) {
		$this->db = $model;
		$this->id = $id;
		$this->message = self::DEFAULT_MESSAGE;
		$this->parameters = $parameters;
		$this->edge_id = $edge_id;
		if(!$memory) {
			$memory = (object)array();
		}
		$this->memory = $memory;
	}
	
	public function getModel() {
		return $this->db;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getEdgeId() {
		return $this->edge_id;
	}
	
	abstract protected function _simulate($timestamp);
	
	public function simulate($timestamp) {
		$result = $this->_simulate($timestamp);
		
		if(isset($result['message'])) {
			$this->message = $result['message'];
		} else {
			$this->message = self::DEFAULT_MESSAGE;
		}
		
		// normalize value.
		$v = min(max($result['result'],self::RESULT_MIN),self::RESULT_MAX);
		return $v;
	}
	
	public function getMessage() {
		return $this->message;
	}
	
	public function getMemory() {
		return $this->memory;
	}
	
}

?>
