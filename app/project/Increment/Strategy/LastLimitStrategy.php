<?php

namespace Increment\Strategy;

class LastLimitStrategy extends Strategy {

	protected function _simulate($timestamp) {
		
		$current = $this->db->getValue($timestamp);
		$limit = $this->memory;
    $frac = $this->parameters->perc / 100;
    
		if((1 + $frac) * $current < $limit) {
			return array(
				'result' => self::RESULT_MAX,
				'message' => 'Sell impuls: ' . $current . ' < ' . $limit
			);
		}
		
		return array(
			'result' => self::RESULT_MIN,
			'message' => 'Nothing to do.'
		);
	}
  
}
?>
