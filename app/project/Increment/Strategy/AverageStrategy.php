<?php

namespace Increment\Strategy;

class AverageStrategy extends Strategy {

	protected function _simulate($timestamp) {
		
		$avg = $this->db->getAverage($timestamp,$this->parameters->window_length);
		$current = $this->db->getValue($timestamp);
		
		//$x = ( 100 + (double)$this->parameters->perc ) * $avg / 100.0;
		
		if($avg != 0 && $current / $avg <= (100 - $this->parameters->perc) / 100.0 ) {
		
//		if($current < $x) {
			return array(
				'result' => self::RESULT_MAX,
				'message' => 'Sell impuls: ' . $current . '/ ' . (string)$avg . ' ' . ' < ' . $this->parameters->perc
			);
		}
		
		return array(
			'result' => self::RESULT_MIN,
			'message' => 'Nothing to do.'
		);
	}
  
}
?>
