<?php

namespace Increment\Db;

use Symfony\Component\Config\Definition\Exception\Exception;

class Instruments extends Abstrct {
  
  /**
   * Returns reversed instrument
   * @param string $instrument_id
   * @return string reversed instrument id
   * @throws Exception if not found
   */
  public function getReversedInstrumentId($instrument_id) {
    $sql = 'SELECT f1.id 
            FROM financial_instruments f1
            JOIN financial_instruments f2
            ON (f1.currency_1_id=f2.currency_2_id
              AND f1.currency_2_id = f2.currency_1_id)
            WHERE f2.id = :instrument';
		$stmt = $this->db->prepare($sql);
		$stmt->execute(array(
			'instrument' => $instrument_id
		));
    $row = $stmt->fetch();
    if (!$row)
      throw new Exception ('Reversed instrument for '.$instrument_id.' not found');
		return $row['0'];
  }
  
  /**
   * Returns row with instrument data given its id
   * @param string $instrument_id
   * @return array instrument data
   * @throws Exception if failed
   */
  public function getInstrumentById($instrument_id) {
    $sql = 'SELECT *
            FROM financial_instruments
            WHERE id = :instrument';
		$stmt = $this->db->prepare($sql);
		$stmt->execute(array(
			'instrument' => $instrument_id
		));
    $row = $stmt->fetch();
    if (!$row)
      throw new Exception ('Exchanger for '.$instrument_id.' not found');
		return $row;
  }
  
  /**
   * Gets exchanger id from array of instruments id,
   * but only if it is the same for all
   * @param array $instruments of instrument ids
   * @return string exchanger id
   * @throws Exception if at least one instrument has different exchanger
   */
  public function getExchangerFromArray($instruments) {
    $inst_0 = $this->getInstrumentById($instruments[0]);
    foreach($instruments as $inst_id) {
      $inst_1 = $this->getInstrumentById($inst_id);
      if ($inst_0['exchanger_id'] !== $inst_1['exchanger_id']) {
        throw new Exception ("Instruments from different exchangers");
      }
    }
    return $inst_0['exchanger_id'];
  }

}
?>
