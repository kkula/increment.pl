<?php

namespace Increment\Db;

abstract class Abstrct {
	/**
	 * @var \PDO PDO Instance
	 */
	protected $db;
	
	public function __construct() {
		$this->db = \Increment\Db::getDb();
	}
	
	protected function _getConfig($el) {
		return \Increment\Db::getConfig($el);
	}
}