<?php

namespace Increment\Db;

use Symfony\Component\Config\Definition\Exception\Exception;
use \PDO;

class Simulation extends Abstrct {
	
	public function getOnline() {
		$sql = 'SELECT * FROM simulations WHERE online = TRUE';
		return $this->db->query($sql)->fetchAll();
	}


	/**
   * Returns ids and names of classes constructors
   * of available graph schemes
   * or FALSE
   * @return array of ids and names
   */
  public function getGraphSchemes() {
    $sql = "SELECT id, name 
            FROM graph_schemes
            WHERE available='t'
            ORDER BY id ASC";
		$stmt = $this->db->prepare($sql);
		$stmt->execute();
    if ($stmt === FALSE)
      return FALSE;
		$res = $stmt->fetchAll();
		return $res;
  }
  
  public function setStatus($id,$value) {
	  $sql = 'UPDATE simulations SET status = :value WHERE id = :id';
	  $stmt = $this->db->prepare($sql);
	  $stmt->execute(array(
		  'value' => $value,
		  'id' => $id
	  ));
  }
  
  /**
   * Returns id of strategy with given name
   * @return integer id
   * @param string $name strategy name
   * @throws Exception on empty set
   */
  public function getStrategyIdByName($name) {
    $sql = 'SELECT id FROM strategies
            WHERE name=:name';
		$stmt = $this->db->prepare($sql);
		$stmt->execute(array(
			'name' => $name
		));
		$row = $stmt->fetch();
    if (!$row)
      throw new Exception('Strategy with name '.$name.' not found');
		return $row['id'];
  }
  
  /**
   * Returns instance of graph scheme with given id
   * @return object extending GraphScheme graph scheme
   * @param integer $id graph scheme id
   * @throws Exception on empty set
   */
  public function getGraphSchemeById($id) {
    $sql = 'SELECT name FROM graph_schemes
            WHERE id=:id';
		$stmt = $this->db->prepare($sql);
		$stmt->execute(array(
			'id' => $id
		));
		$row = $stmt->fetch();
    if (!$row)
      throw new Exception('Graph scheme with id '.$id. ' not found');
		$classname = '\Increment\GraphScheme\\' . $row['name'];
    return new $classname;
  }
  
  /**
   * Creates new user strategy, returns its id
   * @return integer id of strategy
   * @param integer $user_id user id
   * @param integer $strategy_id id of selected strategy
   * @param string $parameters values of parameters in json
   * @throws Exception on failure
   */
  public function createUserStrategy($user_id, $strategy_id, $parameters) {
    
    $sql = 'INSERT INTO user_strategies
            (user_id, strategy_id, parameters)
            VALUES (:user_id, :strategy_id, :parameters)';
		$stmt = $this->db->prepare($sql);
		$res = $stmt->execute(array(
			'user_id' => $user_id,
      'strategy_id' => $strategy_id,
      'parameters' => $parameters
		));
		if (!$res) {
      throw new Exception;
    }
    return $this->db->lastInsertId("user_strategies_id_seq");
  }
  
  /**
   * Creates new graph for given user
   * @return integer graph's id
   * @param integer $user_id
   * @param string $exchanger exchanger id
   * @param string $name graph name (default null)
   * @throws Exception on failure
   */
  public function createGraph($user_id, $exchanger, $name=NULL) {
    $sql = "INSERT INTO graphs
            (user_id, name, exchanger_id)
            VALUES (:user_id, :name, :exchanger)";
		$stmt = $this->db->prepare($sql);
		$res = $stmt->execute(array(
			'user_id' => $user_id,
      'exchanger' => $exchanger,
      'name' => $name
		));
		if (!$res) {
      throw new Exception;
    }
    return $this->db->lastInsertId("graphs_id_seq");
  }
  
  /**
   * Adds edge to graph with given id.
   * @param integer $graph_id selected graph id
   * @param string $instrument_id instrument on which wdge goes
   * @param integer $user_strategy user strategy id on edge
   * @throws Exception on failure
   */
  private function addEdge($graph_id, $instrument_id, $user_strategy) {
    $sql = 'INSERT INTO edges
            (instrument_id, graph_id, user_strategy_id)
            VALUES (:instrument, :graph, :strategy)';
		$stmt = $this->db->prepare($sql);
		$res = $stmt->execute(array(
			'instrument' => $instrument_id,
      'graph' => $graph_id,
      'strategy' => $user_strategy
		));
		if (!$res) {
      throw new Exception;
    }
  }
  
  /** 
   * Adds edge and user strategy basing on description
   * @param integer $user_id owner of strategies and graph
   * @param integer $graph_id graph id
   * @param string $instrument_id id of used instrument
   * @param array $description consisting of field 'strategy'
   * defining name of strategy, and field 'param_map' being array
   * of indexes telling which cells from $param are to chose
   * @param array $param of values
   */
  private function addEdgeUsingDescription(
          $user_id, $graph_id, $instrument_id, $description, $param)
  {
    $param_values = json_encode($description['param_map']);
    $strategy_id = $this->getStrategyIdByName($description['strategy']);
    $u_st_id = $this->createUserStrategy($user_id, $strategy_id, $param_values);
    $this->addEdge($graph_id, $instrument_id, $u_st_id);
  }
  
  /**
   * Creates new simulation, returns its id.
   * @param array config consisting of values under keys: 
   * <ul>
   * <li>integer graph_id : graph id</li>
   * <li>string name : simulation's name</li>
   * <li>string currency : initial currency </li>
   * <li>integer amount : starting amount</li>
   * <li>boolean is_online : is simulation online</li>
   * <li>integer from : start date, will be now if $is_onlione is true </li>
   * <li>integer $to : end date, will be null if $is_online is TRUE</li>
   * </ul>
   * @return integer id of created simulation
   * @throws Exception on failure or if name is occupied
   */
  private function createSimulation($config) {
    
    $name = trim(htmlspecialchars($config['name']));
    if (strlen($name) === 0)
      throw new Exception ('Too short simulation name.');

    $sql = "INSERT INTO simulations
            (name, graph_id, online, initial_currency, 
            initial_amount, status, start_date, end_date)
            VALUES (:name, :graph, :online, :currency, :amount, :status, :from, :to)";
		$stmt = $this->db->prepare($sql);
		$res = $stmt->execute(array(
			'name' => $name,
      'graph' => $config['graph_id'],
      'online' => ($config['is_online'] ?  't' : 'f'),
      'currency' => $config['currency'],
      'amount' => $config['amount'],
      'status' => 0,
      'from' => ($config['is_online']) ? time() : $config['from'],
      'to' => ($config['is_online'] ? null : $config['to'])
		));
		if (!$res) {
      throw new Exception;
    }
    $sql = "SELECT currval('simulations_id_seq')";
		$stmt = $this->db->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetch();
    if (!$row) {
      throw new Exception;
    }
    return $row[0];
  }
  
  /**
   * Creates graph consisting of one edge between
   * currencies of given instrument, with the same
   * strategy (but different parameters) on each edge,
   * and simulation on this graph
   * Returns simulation's id.
   * @param array config consisting of values under keys 
   * <ul>
   * <li>integer user_id : owner of graph</li>
   * <li>array instruments : instruments defining edges</li>
   * <li>object scheme : graph scheme instance</li>
   * <li>array param : parameters for scheme</li>
   * <li>boolean is_online : is simulation online</li>
   * <li>string simulation_name : name for simulation</li>
   * <li>integer amount : starting amount</li>
   * <li>integer from : start date</li>
   * <li>integer to : end date, default null, will be null if is_online is TRUE</li>
   * </ul>
   * @throw Exception on failure
   * @return integer graph's id
   */
  public function createGraphAndSimulationUsingScheme($config) {
    
    $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->db->beginTransaction();
    
    try {      
          
      $f = new Instruments;
      
      $user_id = $config['user_id'];
      $simulation_name = $config['simulation_name'];
      $edges = $config['scheme']->getEdges($config['param']);
      $instruments = $config['instruments'];
      $first_instrument = $f->getInstrumentById($instruments[0]);

      $this->assertVacantSimulationName($user_id, $simulation_name);
      
      /* graph */
      $exchanger_id = $f->getExchangerFromArray($instruments);
      $graph_id = $this->createGraph($user_id, $exchanger_id);
      
      for ($i = 0; $i < count($edges); $i++) {
        $this->addEdgeUsingDescription(
                $user_id, $graph_id, $instruments[$i], $edges[$i], $config['param']);
      }
      
      /* simulation */
      $simulation_id = $this->createSimulation(array(
          'graph_id' => $graph_id,
          'name' => $config['simulation_name'],
          'currency' => $first_instrument['currency_1_id'],
          'amount' => $config['amount'],
          'is_online' => $config['is_online'],
          'from' => $config['from'],
          'to' => $config['to']));
      
    } catch (Exception $e) {
      $this->db->rollBack();
      throw $e;
    }
    
    $this->db->commit();
    return $simulation_id;
  }
  
  /**
   * Throws if there is user's simulation
   * with given name in db
   * @param integer $user_id id of user 
   * @param string $name name of simulation
   * @throw Exception if found
   */
  public function assertVacantSimulationName($user_id, $name) {
    $sql = "SELECT * FROM user_simulations
            WHERE simulation_name = :name
              AND user_id = :user";
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array(
        'name' => $name,
        'user' => $user_id
    ));
    if ($stmt->fetch() !== false) {
      throw new Exception(
              'User '.$user_id.' already has simulation with name '.$name);
    }
  }
  
  public function getSimulationTimes($simulation_id) {
	  $q = 'SELECT start_date, end_date FROM simulations WHERE id = :id';
	  $stmt = $this->db->prepare($q);
	  $stmt->execute(array('id' => $simulation_id));
	  
	  $x = $stmt->fetch();
	  return $x;
  }
  
  public function getCurrentCurrency($simulation_id) {
	  $sql = 'WITH c AS (
SELECT final_currency AS currency FROM transactions WHERE simulation_id = :id ORDER BY timestamp DESC LIMIT 1
) SELECT
	CASE WHEN (SELECT COUNT(*) FROM c) = 1 THEN (SELECT currency FROM c)
	ELSE (SELECT initial_currency FROM simulations WHERE id = :id) END as currency;';
	  $stmt = $this->db->prepare($sql);
	  $stmt->execute(array(
		  'id' => $simulation_id
	  ));
	  
	  $row = $stmt->fetch();
	  return $row['currency'];
  }
  
  public function getCurrentAmount($simulation_id) {
	  $sql = 'WITH c AS (
SELECT final_amount AS amount FROM transactions WHERE simulation_id = :id ORDER BY timestamp DESC LIMIT 1
) SELECT
	CASE WHEN (SELECT COUNT(*) FROM c) = 1 THEN (SELECT amount FROM c)
	ELSE (SELECT initial_amount FROM simulations WHERE id = :id) END as amount;';
	  $stmt = $this->db->prepare($sql);
	  $stmt->execute(array(
		  'id' => $simulation_id
	  ));
	  
	  $row = $stmt->fetch();
	  return $row['amount'];
  }
  
  public function saveTransation($simulation_id,$data) {
	  $sql = 'INSERT INTO transactions
	(simulation_id,timestamp,swap_rate,swap_message,edge_used,final_currency,final_amount) VALUES
	(:sid,:timestamp,:rate,:msg,:edge_id,:currency,:amount)';
	  $stmt = $this->db->prepare($sql);
	  return $stmt->execute(array(
		'sid' => $simulation_id,
		'timestamp' => $data['timestamp'],
		'rate' => $data['rate'],
		'msg' => $data['message'],
		'edge_id' => $data['edge_id'], // TODO: normal edgeId
		'currency' => $data['currency'],
		'amount' => $data['amount']
	  ));
  }
  
  public function getStrategies($simulation_id,$currency) {
	  $sql = 'SELECT * FROM user_strategies_v WHERE simulation_id = :sid AND start_currency = :sc';
	  
	  $stmt = $this->db->prepare($sql);
	  $stmt->execute(array(
		  'sid' => $simulation_id,
		  'sc' => $currency
	  ));
	  
	  return $stmt->fetchAll();
  }
  
  /**
   * Returns all simulations of givaen user
   * in order of adding
   * @param integer $user_id Owner of simulations
   * @return mixed array User simulations or FALSE
   */
  public function getSimulations($user_id) {
	$sql = 'SELECT simulations.* FROM simulations
          JOIN graphs ON simulations.graph_id = graphs.id
          WHERE graphs.user_id = :user_id
          ORDER BY simulations.id';
	$stmt = $this->db->prepare($sql);
	$stmt ->execute(array(
		'user_id' => $user_id
	));
	return $stmt->fetchAll();
  }

  public function getSimulation($user_id, $sim_id) {
    $sql = 'SELECT s.*
            FROM simulations s
            JOIN graphs g ON s.graph_id = g.id
            WHERE g.user_id = :user_id
              AND s.id = :sim_id';
    $stmt = $this->db->prepare($sql);
    $stmt ->execute(array(
      'user_id' => $user_id,
      'sim_id' => $sim_id
    ));
    return $stmt->fetch();
  }

  public function getTransactions($sim_id, $after=null) {
    $sql = 'SELECT timestamp, swap_message AS message, 
              swap_rate AS rate, instrument_id AS instrument,
              final_amount AS amount, 
              final_amount/initial_amount AS perc_change
            FROM transactions t
            JOIN simulations s ON t.simulation_id = s.id
            JOIN edges e ON t.edge_used = e.id
            WHERE s.id = :sim_id';
    
    $arr = array('sim_id' => $sim_id);
    
    if ($after != null) {
      $sql .= ' AND timestamp > :after';
      $arr['after'] = $after;
    }
    
    $sql .= ' ORDER BY timestamp';
    
    $stmt = $this->db->prepare($sql);
    $stmt ->execute($arr);
    return $stmt->fetchAll();
  }
  
  public function getStartingInstrument($sim_id) {
    $sql = 'SELECT f.id
            FROM simulations s
            JOIN graphs g ON s.graph_id = g.id
            JOIN edges e ON g.id = e.graph_id
            JOIN financial_instruments f ON f.id = e.instrument_id
            WHERE s.id = :sim_id
              AND f.currency_1_id = s.initial_currency';
    $stmt = $this->db->prepare($sql);
    $stmt ->execute(array(
      'sim_id' => $sim_id
    ));
    $res = $stmt->fetch();
    if ($res === false)
      return false;
    return $res['id'];
  }
  
  public function removeSimulation($simulation_id) {
  	$sql = 'DELETE FROM simulations where id = :simulation_id';
  	$stmt = $this->db->prepare($sql);
  	$stmt ->execute(array(
  	'simulation_id' => $simulation_id
  	));
  	return $stmt->fetchAll();
  }
  
  public function stopOnlineSimulation($simulation_id) {
  	$sql = 'UPDATE simulations 
            SET online = FALSE,
            end_date = :end
            WHERE id = :simulation_id';
  	$stmt = $this->db->prepare($sql);
  	$stmt ->execute(array(
      'simulation_id' => $simulation_id,
      'end' => time()
  	));
  	return $stmt->fetchAll();
  }
  
  public function saveMemory($usId,$memory) {
	  $sql = 'UPDATE user_strategies SET memory = :memory WHERE id = :id';
	  $stmt = $this->db->prepare($sql);
	  $stmt->execute(array(
		  'memory' => $memory,
		  'id' => $usId
	  ));
  }
  
  /**
   * @return integer id of user strategy reverse to given 
   * @param integer $usId
   */
  public function getReverseUserStrategyId($usId) {
    $sql = 'SELECT v1.us_id
            FROM user_strategies_v v1
              JOIN user_strategies_v v2
              ON (v1.simulation_id = v2.simulation_id
                AND v1.start_currency = v2.end_currency
                AND v1.end_currency = v2.start_currency)u.id = e.user_strategy_id)
            WHERE v2.us_id = :us_id';
    $stmt = $this->db->prepare($sql);
    $stmt ->execute(array(
      'us_id' => $usId
    ));
    $res = $stmt->fetch();
    if ($res === false)
      return false;
    return $res['us_id'];
  }
}

?>
