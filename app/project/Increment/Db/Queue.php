<?php

namespace Increment\Db;

use \PDO;

class Queue extends Abstrct {
	public function add($sid,$principial, $ts = null) {
		$sql = 'INSERT INTO queue(simulation_id,principal,timestamp) VALUES (:sid,:pr,:ts)';
		$stmt = $this->db->prepare($sql);
		$stmt->execute(array(
			'sid' => $sid,
			'pr' => $principial,
			'ts' => $ts
		));
	}
	
	public function get($id) {
		$sql = 'SELECT * FROM queue WHERE id = :id';
		$stmt = $this->db->prepare($sql);
		$stmt->execute(array('id' => $id));
		return $stmt->fetch();
	}
	
	public function remove($id) {
		$sql = 'DELETE FROM queue WHERE id = :id';
		
		$stmt = $this->db->prepare($sql);
		
		$stmt->execute(array('id' => $id));
	}
}

?>
