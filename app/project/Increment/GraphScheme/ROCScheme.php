<?php

namespace Increment\GraphScheme;

use Symfony\Component\Config\Definition\Exception\Exception;

class ROCScheme extends GraphScheme {
  
  public function getName() {
    return "Strategy: ROC indicator";
  }
  
  public function getDescription() {
    return "
      Strategy evaluates MA(ROC(w1,w2,n),m) - ROC(w1,w2,n) indicator.
      If it changes from positive to negative, strategy makes transaction 
      from <em>w<sub>1</sub></em> to <em>w<sub>2</sub></em>.
      Reverse transaction is made whenever indicator's value changes
      from negative to positive.
    ";
  }

  public function getParameters() {
    return array("n", "m");
  }

  private function properValue($x) {
    return $x > 0 && $x < 10000000;
  }
  
  public function getEdges($params) {
	  
	  //Here is a place for a potential conversion of time frame:
	  $n = doubleval($params[0]);
	  $m = doubleval($params[1]);
	  
    if (!$this->properValue($n) || !$this->properValue($m))
      throw new Exception();
	  
    return array(
        array(
            'strategy' => 'ROC',
            'param_map' => array(
				'n' => $n,
				'm' => $m
			)
        ),
        array(
            'strategy' => 'ROC',
            'param_map' => array(
				'n' => $n,
				'm' => $m
			)
        )
    );
  }
}
?>
