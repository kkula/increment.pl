<?php

namespace Increment\GraphScheme;

use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * 
 */
abstract class GraphScheme {
  
  /**
   * Returns schema name
   * @return string Name
   */
  abstract public function getName();
  
  /**
   * Returns schema and its parameters description
   * @return string Description
   */
  abstract public function getDescription();
  
  /**
   * Returns schema parameters as an array of names
   * @return array Parameters
   */
  abstract public function getParameters();
  
  /**
   * Returns description of edges
   * @return array consisting of array for each edge.
   * Each edge array has two fields: 'strategy' defining
   * which strategy should be bound to this edge,
   * and 'param_map', which is array of indexes pointing
   * to which of parameters should be bound to edge,
   * provided they are in the same order as in getParameters.
   * @throws Exception if at lest one parameter is invalid.
   */
  abstract public function getEdges($params);
  
}

?>
