<?php

namespace Increment\GraphScheme;

use Symfony\Component\Config\Definition\Exception\Exception;

class LimitScheme extends GraphScheme {
  
  public function getName() {
    return "Strategy: Limit";
  }
  
  public function getDescription() {
    return 
    "<p>This strategy recieves 2 arguments:
      <em>l<sub>1</sub></em> (limit 1) and <em>l<sub>2</sub></em> (limit 2).
      </p><p>
      It makes a transaction from currency
      <em>c<sub>1</sub></em> to currency <em>c<sub>2</sub></em>
      if its rate falls below <em>l<sub>1</sub></em>.
      Similary, it makes reverse transaction, if rate
      of <em>c<sub>1</sub></em> falls below <em>l<sub>2</sub></em>.
      </p>
      ";
  }

  public function getParameters() {
    return array("Limit 1", "Limit 2");
  }
  
  private function properValue($x) {
    return $x > 0 && $x < 100;
  }
  
  public function getEdges($params) {
    
    if (!$this->properValue($params[0]) || !$this->properValue($params[1]))
      throw new Exception();
    
    return array(
        array(
            'strategy' => 'Limit',
            'param_map' => array(
            'limit' => doubleval($params[0])
			)
        ),
        array(
            'strategy' => 'Limit',
            'param_map' => array(
            'limit' => 1.0 / doubleval($params[1])
        )
      )
    );
  }
}
?>
