<?php

namespace Increment\GraphScheme;

use Symfony\Component\Config\Definition\Exception\Exception;

class MovingAverageScheme extends GraphScheme {
  
  public function getName() {
    return "Strategy: Moving Average";
  }
  
  public function getDescription() {
    return "
      <p>This strategy makes a transaction beetwen currency
      <em>c<sub>1</sub></em> and <em>c<sub>2</sub></em>
      each time rate of <em>c<sub>2</sub></em>
      is at least <em>a<sub>1</sub></em>% lower
      then its average rate from last <em>d</em> minutes.
      However, it discards all rates at least <em>x</em>% higher than
      current rate. Moreover, transaction isn't done if there occured
      at least <em>y</em> minutes long period with all rates discarded
      during last <em>d</em> minutes. 
      Strategy makes reverse transaction whenever rate is at least
      <em>a<sub>2</sub></em> higher than rate of last transaction.
    ";
  }

  public function getParameters() {
    return array("d", "x", "y", "a<sub>1</sub>",  "a<sub>2</sub>");
  }

  private function properPercValue($p) {
    return $p > 0 && $p < 1000;
  }
  
  // year in minutes
  private function properLengthValue($l) {
    return $l > 0 && $l < 365 * 24 * 60 * 60;
  }
  
  public function getEdges($params) {
	  
    if (!$this->properLengthValue($params[0]) || !$this->properLengthValue($params[2])
            || !$this->properPercValue($params[1]) || !$this->properPercValue($params[3])
            || !$this->properPercValue($params[4]))
      throw new Exception;
    
	  $length = doubleval($params[0]) * 60; // $params[0] is in minutes.
	  $y_length = doubleval($params[2]) * 60;
    
    
    return array(
        array(
            'strategy' => 'ImprovedAverage',
            'param_map' => array(
              'perc' => doubleval($params[3]),
              'discard_perc' => doubleval($params[1]),
              'window_length' => $length,
              'discard_length' => $y_length
            )
        ),
        array(
            'strategy' => 'LastLimit',
            'param_map' => array(
            'perc' => doubleval($params[4])
            )
        )
    );
  }
}
?>
