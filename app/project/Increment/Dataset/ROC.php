<?php

namespace Increment\Dataset;

class Shift extends Abstrct {
	
	private $ds1=null,$shift=null;


	public function __construct(Abstrct $dataset1,$shift) {
		$this->ds1 = $dataset1;
		$this->shift = (int)$shift;
	}
	
	protected function getDatasetSql() {
		// TODO: to check if key is from d1 or from d2.
		$s1 = $this->ds1->getDatasetSql();
		$sql = 'SELECT key+' . $this->shift . ' AS key, value FROM (' . $s1 . ') AS x';
		return $sql;
	}
	
	public function getExpression() {
		return 'SHIFT(' . $this->ds1->getExpression() . ', ' . $this->shift . ')';
	}
}