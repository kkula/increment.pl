<?php

namespace Increment\Dataset;

abstract class Abstrct {
	
	static private $db = null;
	
	/**
	 * Returns PDO
	 * @return \PDO
	 */
	static protected function getDb() {
		return \Increment\Db::getDb();
	}
	
	abstract public function getExpression();
	abstract protected function getDatasetSql();
	
	public function getValue($key) {
		$sql = 'SELECT value FROM (' . $this->getDatasetSql() . ') as x WHERE key <= :key ORDER BY key DESC LIMIT 1';
		$db = self::getDb();
		$stmt = $db->prepare($sql);
		$stmt->execute(array(
			'key' => $key
		));
		$row = $stmt->fetch();
		if($row)
			return (double)$row['value'];
		return null;
	}
	
	public function getAll() {
		$sql = 'SELECT key,value FROM ( ' . $this->getDatasetSql() . ') as x ORDER BY key';
		$db = self::getDb();
		return $data = $db->query($sql)->fetchAll();
	}
	
	public function getAfter($timestamp) {
		$sql = 'SELECT key,value FROM ( ' . $this->getDatasetSql() . ') as x WHERE key > :ts ORDER BY key';
		$db = self::getDb();
		$stmt = $db->prepare($sql);
		$stmt->execute(array(
			'ts' => $timestamp
		));
		return $data = $stmt->fetchAll();
	}
	
	/**
	 *  Just for tests. Not to use in any other cases!
	 * @return string
	 */
	public function getSql() {
		return $this->getDatasetSql();
	}
}