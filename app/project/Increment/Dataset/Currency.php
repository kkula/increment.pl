<?php

namespace Increment\Dataset;

class Currency extends Abstrct {
	
	private $currencyName = null;
	
	static protected $data = array();
	static protected $keys = array();
	static protected $maxKey = 0;
	static protected $minKey = 9999999999;
	static protected $dsMax = array();
	
	protected function _getValue($key) {
		if( !isset(self::$data[$this->currencyName]) ) {
			self::$data[$this->currencyName] = array();
			self::$dsMax[$this->currencyName] = 0;
		}
		
		if(self::$dsMax[$this->currencyName] < $key) {
			$this->_getDataset($key);
		}
		
		$el = null;
		
		foreach(self::$data[$this->currencyName] as $k => $value) {
			if($k <= $key)
				$el = $value;
			else {
				return $el;
			}
		}
		
		
		return 0;
	}
	
	public function getValue($key) {
		$x = $this->_getValue($key);
		return $x;
	}
	
	protected function _getDataset($timestamp,$cnt = null) {
		
		$time = microtime(true);
		
		$count = 1000;
		if($cnt)
			$count = (int)$cnt;
		$sql = 'SELECT * FROM rates WHERE instrument_id = :iid AND timestamp >= :timestamp - 1000 ORDER BY timestamp LIMIT :limit';
		
		$db = self::getDb();
		$stmt = $db->prepare($sql);
		$stmt->execute(array(
			'iid' => $this->currencyName,
			'timestamp' => $timestamp,
			'limit' => $count
		));
		
		
		if(!isset(self::$data[$this->currencyName])) {
			self::$data[$this->currencyName] = array();
			self::$dsMax[$this->currencyName] = array();
		}
		
		foreach($stmt as $x) {
			self::$data[$this->currencyName][$x['timestamp']] = $x['value'];
			self::$dsMax[$this->currencyName] = max(self::$dsMax[$this->currencyName],$x['timestamp']);
			self::$keys[] = $x['timestamp'];
			self::$maxKey = max(self::$maxKey,$x['timestamp']);
			if(!self::$minKey)
				self::$minKey = $x['timestamp'];
			self::$minKey = min(self::$minKey,$x['timestamp']);
		}
		
		// ksort
		ksort(self::$data[$this->currencyName]);
		self::$keys = array_unique(self::$keys);
		sort(self::$keys);
		
		\Increment\Db\Log::log('Currency->_getDataset(): ' . (microtime(true) - $time));
		
	}
	
	protected function _getKeys($timestamp,$cnt = null) {
		
		$time = microtime(true);
		
		
		$count = 10000;
		if($cnt)
			$count = (int)$cnt;
		$sql = 'SELECT timestamp FROM rates WHERE timestamp >= :timestamp ORDER BY timestamp LIMIT :limit';
		
		$db = self::getDb();
		$stmt = $db->prepare($sql);
		$stmt->execute(array(
			'timestamp' => $timestamp,
			'limit' => $count
		));
		
		foreach($stmt as $x) {
			self::$keys[] = $x['timestamp'];
			self::$maxKey = max(self::$maxKey,$x['timestamp']);
		}
		
		
		self::$keys = array_unique(self::$keys);
		sort(self::$keys);
		
		\Increment\Db\Log::log('Currency->_getKeys(): ' . (microtime(true) - $time));
	}

		public function __construct($currencyName) {
		$this->currencyName = preg_replace("/[^A-Za-z0-9 ]/", '', $currencyName);
	}
	
	protected function getDatasetSql() {
		return 'SELECT timestamp as key, value FROM rates WHERE instrument_id = \'' . $this->currencyName . '\'';
	}
	
	public function getExpression() {
		return  'CURRENCY(' . $this->currencyName . ')';
	}
	
	public function getResultCurrency() {
		return substr($this->currencyName,-3,3);
	}
	
  protected function _getAvg($timestamp,$length) {
    return $this->_getFilteredAvg($timestamp, $length).get('avg');
  }
  
  protected function _isToDiscardValue($v, $discardLimit, $discardPerc) {
    if ($discardLimit === null) return false;
    return $v * 100 > (100 + $discardPerc) * $discardLimit;
  }
  
	protected function _getFilteredAvg($timestamp,$length,$discardLimit=null, $discardPerc=null) {
    
		$delta = 1000;
		if(self::$minKey > $timestamp - $length - $delta || !isset(self::$data[$this->currencyName])) {
			$x = $timestamp - $length - $delta;
			$this->_getDataset($x, $timestamp - $x + $delta);
		}
    
		// iterate over timestamp
		$prevKey = null;

		$sum = 0.0;
		$timeSum = 0.0;
    
    $discardRange = 0.0;
    $maxDiscardRange = 0.0;
    
		foreach(self::$data[$this->currencyName] as $key => $value) {
			if(!$prevKey) {
				$prevKey = $key;
				$prevValue = $value;
			}

      $isToDiscard = $this->_isToDiscardValue($prevValue, $discardLimit, $discardPerc);
      
			if($key >= $timestamp - $length && $key <= $timestamp) {
				$timeRange = $key - max($prevKey, $timestamp - $length);
				
        if ($isToDiscard) {
          $discardRange += $timeRange;
          $maxDiscardRange = max($maxDiscardRange, $discardRange);
        } else {
          $discardRange = 0;
          $sum += $timeRange * $prevValue;
				
          $timeSum += $timeRange;
				
          $sum += ($key - max($prevKey,$timestamp-$delta)) * $prevValue;
          $timeSum += ($key - max($prevKey,$timestamp-$delta) );
        }
			}

			if($key > $timestamp && $prevKey < $timestamp) {
				$range = $timestamp - $prevKey;
        if ($isToDiscard) {
          $discardRange += $timeRange;
          $maxDiscardRange = max($maxDiscardRange, $discardRange);
        } else {
  				$sum += $range * $prevValue;
    			$timeSum += $range;
        }
			}

			$prevKey = $key;
			$prevValue = $value;
		}


		if(!$timeSum)
			$avg = $this->getValue($timestamp);
		else
      $avg = $sum / (double)$timeSum;
    
    return array('avg' => $avg, 'discarded' => $maxDiscardRange);
	}
	
  public function getFilteredAverage($timestamp, $length, $discardLimit, $discardPerc) {
    return $this->_getFilteredAvg($timestamp, $length, $discardLimit, $discardPerc);
  }
  
	public function getAverage($timestamp,$length) {
		
		
		return $this->_getAvg($timestamp, $length);
		
		
		
		// Old SQL: Much more readable, but SLOW.
		$sql = 'WITH cte1 AS (
			SELECT timestamp,value
			FROM rates
			WHERE instrument_id = :instrument
				AND timestamp < :ts
				AND timestamp >= :ts2
		),
		cte3 AS (
			SELECT *,
				(SELECT timestamp
				FROM cte1 AS x
				WHERE x.timestamp > y.timestamp
				ORDER BY timestamp
				LIMIT 1) as ts2
			FROM cte1 AS y
		)
		SELECT sum(diff*value)/sum(diff) as value
		FROM (SELECT ts2 - timestamp AS diff, value FROM cte3) as x';
		
		
		// equivalent of SQL above but faster.
		$sql = 'SELECT sum(diff*value)/sum(diff) as value
		FROM (SELECT ts2 - timestamp AS diff, value FROM (

SELECT *,
				(SELECT timestamp
				FROM (

SELECT timestamp,value
			FROM rates
			WHERE instrument_id = :instrument
				AND timestamp < :ts
				AND timestamp >= :ts2

				) AS x
				WHERE x.timestamp > y.timestamp
				ORDER BY timestamp
				LIMIT 1) as ts2
			FROM (

SELECT timestamp,value
			FROM rates
			WHERE instrument_id = :instrument
				AND timestamp < :ts
				AND timestamp >= :ts2

			) AS y

		) AS x) as x';
		
		$db = self::getDb();
		$stmt = $db->prepare($sql);
		$stmt->execute(array(
			'instrument' => $this->currencyName,
			'ts' => $timestamp,
			'ts2' => $timestamp - $length
		));
		
		
		$row = $stmt->fetch();
		return (double)$row['value'];
	}
	
	public function getNextTick($from,$depth = 0) {
		
		if($depth > 5)
			return 0;
		
		if(self::$maxKey <= $from) {
			$this->_getKeys($from);
		}
		
		if($depth > 0) {
			$this->_getKeys($from,1000 * pow(10,$depth));
		}
		
		$check = 1362749942;
		
		$ret = 0;
		foreach(self::$keys as $v) {
			if($v > $from) {
				$ret = $v;
				break;
			}
		}
		
		if($ret == 0) {
			return $this->getNextTick($from,$depth+1);
		}
		
		return $ret;
		
		$sql = 'SELECT timestamp FROM rates WHERE timestamp > :from ORDER BY timestamp LIMIT 1';
		$db = self::getDb();
		
		$stmt = $db->prepare($sql);
		$stmt->execute(array(
			'from' => $from
		));
		
		$x = $stmt->fetch();
		return $x['timestamp'];
	}
}