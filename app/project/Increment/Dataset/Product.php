<?php

namespace Increment\Dataset;

class Product extends Abstrct {
	
	private $ds1=null,$ds2=null;


	public function __construct(Abstrct $dataset1,Abstrct $dataset2) {
		$this->ds1 = $dataset1;
		$this->ds2 = $dataset2;
	}
	
	private function _partialSql($s1,$s2) {
		return 'SELECT d1.key, d1.value *' .
		'( SELECT d2.value FROM (' . $s1 . ' ) as d2 WHERE d2.key<=d1.key ORDER BY key DESC LIMIT 1 ) as value '.
		'FROM ( ' . $s2 . ' ) AS d1 ';
	}


	protected function getDatasetSql() {
		// TODO: to check if key is from d1 or from d2.
		$s1 = $this->ds1->getDatasetSql();
		$s2 = $this->ds2->getDatasetSql();
		$sql = $this->_partialSql($s1,$s2) . ' union all ' . $this->_partialSql($s2, $s1);
		return $sql;
	}
	
	public function getExpression() {
		return $this->ds1->getExpression() . ' * ' . $this->ds2->getExpression();
	}
}