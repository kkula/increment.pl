<?php

namespace Increment\Dataset;

class Constant extends Abstrct {
	
	private $value = 1;


	public function __construct($value = 1) {
		$this->value = (double)$value;
	}
	
	protected function getDatasetSql() {
		return 'SELECT 0 as key, ' . $this->value . ' as value';
	}
	
	public function getExpression() {
		return $this->value;
	}
}