<?php

use Symfony\Component\HttpFoundation\Request;

$subapp = $app['controllers_factory'];

$subapp->get('/authors',function() use ($app) {
	
	$authors = array(
			'Kacper Kula',
			'Kasia Jabłonowska',
			'Witold Jędrzejewski',
			'Bartłomiej Gajewski'
		);
	
		shuffle($authors);
	
	return $app->json(array(
		'status' => 0,
		'data' => $authors 
	));
});

$getInstrument = function($instrument_id, $timestamp = null) use ($app) {
	
	
	$reversedInstument = substr($instrument_id,0,3) . substr($instrument_id,6,3) . substr($instrument_id,3,3);
	
	
	if(!$timestamp) {
		$data = $app->db->currency($instrument_id)->getAll();
		$data2 = $app->db->currency($reversedInstument)->getAll();
	} else {
		$data = $app->db->currency($instrument_id)->getAfter($timestamp);
		$data2 = $app->db->currency($reversedInstument)->getAfter($timestamp);
	}
	
	$y = array();
	
	foreach($data as $x) {
		$y[$x['key']] = array(
			'value1' => (double)$x['value'],
			'value2' => null
		);
	}
	
	foreach($data2 as $x) {
		if(isset($y[$x['key']])) {
			$y[$x['key']]['value2'] = 1.0/(double)$x['value'];
		} else {
			$y[$x['key']] = array(
				'value1' => null,
				'value2' => 1.0/(double)$x['value']
			);
		}
	}
	
	return $app->json($y);
	
	
	$r = array();
	foreach($data as $x) {
		$r[] = array($x['key']*1000,(double)$x['value']);
	}
	
	return $app->json(array(
		'count' => count($data),
		'data' => $r
	));
};


$subapp->get('/currency/get/{instrument_id}',function($instrument_id) use ($getInstrument) {
	return $getInstrument($instrument_id);
});

$subapp->get('/currency/get/{instrument_id}/{after}',function($instrument_id,$after) use ($getInstrument) {
	return $getInstrument($instrument_id,$after);
});

$subapp->get('/logout', function() use ($app) {
	$app['session']->clear();
	return $app->redirect('/');
});

$subapp->get('/change_password/{old_pass}/{pass}', 
			function($old_pass, $pass) use ($app) {
	
	$user = $app['session']->get('user');
	$result = $app->db->user->
				changePassword($user['id'], $old_pass, $pass);
	return $app->json($result);
})
->before($loggedOnly);

$subapp->post('/check_password/', function() use ($app) {
	$pass = $app['request']->get('old_pass');
	$user = $app['session']->get('user');
	$result = $app->db->user->checkPassword($user['id'], $pass);
	return $app->json($result);
})
->before($loggedOnly);

$subapp->post('/check_simulation_name/', function() use ($app) {
  $user_id = $app['session']->get('user');
  $name = $app['request']->get('creator-name');
  try {
    $app->db->simulation->assertVacantSimulationName($user_id, $name);
  } catch (Exception $e) {
    return $app->json(FALSE);
  }
  return $app->json(TRUE);
})
->before($loggedOnly);

$to_timestamp = function($v) {
  return ceil($v/1000);
};

$subapp->post('/simulate/{inst}/{from}/{to}', function($inst, $from, $to) use ($app) {
      
  $user = $app['session']->get('user');
  $name = $app['request']->get('creator-name');
  $amount = $app['request']->get('creator-amount');
  $strategy = $app['request']->get('creator-strategy');
  $type = $app['request']->get('creator-type');
  $is_online = ($type == 'online');
  
  try {
    $app->db->simulation->assertVacantSimulationName($user['id'], $name);
  } catch (Exception $e) {
    return $app->json(array(
        "success"=>false, 
        "message"=>"You already have simulation by this name."));
  }
  try {
      
    //getting selected scheme instance
    $scheme = $app->db->simulation->getGraphSchemeById($strategy);
    
    //getting parameters
    $param_count = count($scheme->getParameters());
    $param = array();
    for ($i = 1; $i <= $param_count; $i++) {
      $param[] = $app['request']->get('parametr-'.$strategy.'-'.$i);
    }

    $rev_inst = $app->db->instruments->getReversedInstrumentId($inst);

    //creating graph and simulation
    $simulation_id = $app->db->simulation->createGraphAndSimulationUsingScheme(array(
        'user_id' => $user['id'],
        'instruments' => array($inst, $rev_inst),
        'scheme' => $scheme,
        'param' => $param,
        'is_online' => $is_online,
        'simulation_name' => $name,
        'amount' => $amount,
        'from' => $from,
        'to' => $to));
	
	if(!$is_online) {
		$app->db->queue->add($simulation_id,'historical');
	}
	
  } catch (Exception $e) {
    return $app->json(array(
        'status' => false, 
        'message' => 'Invalid simulation parameters'+$e->getMessage()));
  }
  return $app->json(array('status' => true, 'simulation_number' => $simulation_id));

}) //TODO assert existing period,
->convert('from', $to_timestamp)
->convert('to', $to_timestamp)
->before($loggedOnly);

$subapp->get('/simulations', function() use ($app) {
		$user = $app['session']->get('user');
		$simulations = $app->db->simulation->getSimulations($user['id']);
		return $app->json($simulations);
	}
)
->before($loggedOnly);

$subapp->get('/simulation/{id}/{after}', function($id, $after) use ($app) {
  $user = $app['session']->get('user');
  $sim = $app->db->simulation->getSimulation($user['id'], $id);
  if ($sim === false) {
    $res = array('status' => false);
  }
  else {
    $inst = $app->db->simulation->getStartingInstrument($id);
    $transactions = $app->db->simulation->getTransactions($id, $after);
    $data = array();
    foreach ($transactions as $row) {
      $data[] = array(
          'timestamp' => $row['timestamp'],
          'rate' => $row['rate'],
          'message' => $row['message'],
          'instrument' => $row['instrument'],
          'perc_change' => $row['perc_change'],
          'amount' => $row['amount']
      );
    }
    $res = array(
        'status' => $sim['status'],
        'name' => $sim['name'],
        'instrument' => $inst,
        'type' => ($sim['online'] == 't') ? 'online' : 'offline',
        'from' => $sim['start_date'],
        'to' => $sim['end_date'],
        'data' => $data
    );
  }
  return $app->json($res);
})
->value('after', null)
->before($loggedOnly);

$subapp->post('/remove_simulation/{id}', function($id) use ($app) {
	$user = $app['session']->get('user');
	$sim =  $app->db->simulation->getSimulation($user['id'], $id);
	if ($sim === false) {
   return $app->json(array('status' => false, 'message' => 'Error while removing simulation'));
  }
  else {
	$name = $sim['name'];
	$app->db->simulation->removeSimulation($id);
    return $app->json(array(
        'status' => true, 
        'message' => "OK",
        'simulation_number' => $id,
        'simulation_name' => $name
       ));
       }
})
->before($loggedOnly);

$subapp->post('/simulation/stop/{id}', function($id) use ($app) {
	$user = $app['session']->get('user');
	$sim =  $app->db->simulation->getSimulation($user['id'], $id);
	if ($sim === false) {
   return $app->json(array('status' => false, 'message' => 'Error in api/simulation/stop'));
  }
  else {
    $app->db->simulation->stopOnlineSimulation($id);
      return $app->json(array(
          'status' => true, 
          'message' => "OK"
      ));
  }
})
->before($loggedOnly);

return $subapp;
