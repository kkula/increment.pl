<?php

$subapp = $app['controllers_factory'];

$subapp->get('/',function() use ($app) {
  $user = $app['session']->get('user');
  if ($user === NULL) {
    $app->view->logged = false;
    return $app->view_name = 'home';
  }
  $app->view->logged = true;
	$app->view->user = $user;
  $app->view->default_instrument = $app['chart']['default_instrument'];
  $app->view->instruments = $app['instruments'];
  $app->view->simulations = $app->db->simulation->getSimulations($user['id']);
  $schemes = $app->db->simulation->getGraphSchemes();
  $strategy = array();
  foreach ($schemes as $scheme) {
    $id = $scheme['id'];
    $name = $scheme['name'];
    $classname = "\Increment\GraphScheme\\".$name;
    $object = new $classname;
    $strategy[] = array(
        "id" => $id,
        "name" => $object->getName(),
        "description" => $object->getDescription(),
        "parameters" => $object->getParameters()
    );
  }
  $app->view->strategy = $strategy;
	return $app->view_name = 'dashboard';
});

$subapp->get('/profile',function() use ($app) {
	$user = $app['session']->get('user');
	$hashed_mail = md5(strtolower(trim($user['mail'])));
	$avatar_src = "http://www.gravatar.com/avatar/".$hashed_mail;
	$app->view->avatar_src = $avatar_src;
	return $app->view_name = 'profile';
})
->before($loggedOnly);

$subapp->get('/login',function() use ($app) {
	$app->view->wrong = false;
	return $app->view_name = 'login';
})
->before($notLoggedOnly);

$subapp->post('/login', function() use ($app) {
	$mail = $app['request']->get('mail');
	$pass = $app['request']->get('password');
	$user = $app->db->user->getAuthorizedUser($mail, $pass);
	if ($user === false) {
		$app->view->wrong = true;
		return $app->view_name = 'login';
	}
	unset($user['password']);
	$app['session']->set('user', $user);
	return $app->redirect('/');
})
->before($notLoggedOnly);


$subapp->get('/register',function() use ($app) {
	$app->view->errno = array(-1, array());
	$app->view->name = "";
	$app->view->surname = "";
	$app->view->mail = "";
	
	return $app->view_name = 'register';
})
->before($notLoggedOnly);

$subapp->post('/register', function() use ($app) {
	$app->view->name = $name = $app['request']->get('name');
	$app->view->surname = $surname = $app['request']->get('surname');
	$app->view->mail = $mail = $app['request']->get('email');
	$pass = $app['request']->get('pass1');
	
	$app->view->errno = array(0, array());
	
	$user = $app->db->user->getUserByMail($mail);
	if ($user !== false) {
		array_push($app->view->errno[1], 
			array("An account already exists for the provided e-mail.", "error")
		);	//account already registered for this mail
		$app->view->errno[0]=1;
	}
	
	$validMail = $app->db->user->isValidEmail($mail);
	if ($validMail === false) {
		array_push($app->view->errno[1], 
			array("The provided e-mail is not valid", "error")
		);	//Invalid mail
		$app->view->errno[0]=1;
	}
	
	if ($app->view->errno[0] != 0) {
		return $app->view_name = '/register';
	}
	
	$success = $app->db->user->createAccount($name, $surname, $mail, $pass);
	
	if ($success === false) {
		array_push($app->view->errno[1], 
			array("Server error", "error")
		);	//Failed to register new account
		$app->view->errno[0]=1;
	}
	
	return $app->view_name = '/register';
})
->before($notLoggedOnly);

return $subapp;
