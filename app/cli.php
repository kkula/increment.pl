<?php

$app = require 'app.php';

$type = $argv[1];



if($type == 'h') {
	
	$id = $argv[2];
	
	$time = microtime(true);
	
	echo $id . "\n";
	// getting data from queue
	$queue = $app->db->queue->get($id);
	
	set_time_limit(10000);
	
	$simulator = new \Increment\Simulator\BasicSimulator($queue['simulation_id']);
	$simulator->simulate();
	
	$app->db->simulation->setStatus($queue['simulation_id'],100);
	$app->db->queue->remove($id);
	echo 'Simulation done';
	
	
	\Increment\Db\Log::log('cli.php h ' . $id . ': ' . (microtime(true) - $time));
	
	// executing simulation
	
	// ending
	
} else if($type =='o') {
	
	$time = $argv[2];
	
	// getting all online simulations
	
	$db = new \Increment\Db\Simulation();
	$simulations = $db->getOnline();
	
	foreach($simulations as $s) {
		echo 'Computing: ' . $s['id'] . "\n";
		$simulation = new \Increment\Simulator\BasicSimulator($s['id']);
		$simulation->makeStep($time,true);
	}
	
	
} else {
	die('ERROR');
}