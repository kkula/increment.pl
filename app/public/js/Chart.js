var Chart = function(){
	
	var activeSimulationId = null;
  var activeSimulationIsFinished = false;
	var config = {};
	var chart = null;
	var series = null;
	var timestamp_max = null; 
  
  /* last transaction number */
  var last_tr;
  
	/* last transaction timestamp */
  var tr_timestamp_max = null;
  
	var getData = function(after) {
		$.ajax({
			url: '/api/currency/get/' + config.instrument,
			dataType: 'JSON',
			success: function(data) {
				timestamp_max = null;
				
				for(key in data) {
					timestamp_max = Math.max(timestamp_max,key);
				}
				
				after(data);
			}
		});
	};
	
	var getFromData = function(data,ind) {
		var x = [];
		for(i in data) {
			if(data[i]['value' + ind] === null) continue;
			x.push([i*1000,data[i]['value' + ind]]);
		}
		return x;
	}
	
  var localTime = function(t) {
    var d = new Date(t * 1000);
    return (t - d.getTimezoneOffset() * 60) * 1000;
  }
  
	var fetchNewData = function() {
		var url = '/api/currency/get/'+config.instrument + '/' + timestamp_max;
		$.ajax({
			url: url,
			dataType: 'JSON',
			success: function(data) {
				
				for(key in data) {
          var t = localTime(key);
					timestamp_max = Math.max(timestamp_max,t);
					chart.series[0].addPoint([t,data[key].value1]);
					chart.series[1].addPoint([t,data[key].value2]);
				}
				
			}
		});
  }
  
  var getFlags = function(f , series) {
    return {
      type: "flags",
      data : f,
      onSeries : series,
      shape: 'circlepin',
      width : 16
    };
  }
	
	var redraw = function(data) {
		chart = new Highcharts.StockChart({
			chart: {
				backgroundColor: null,
				renderTo: 'chart'
			},

			rangeSelector : {
				selected : 5,
				
				buttons: [{
					type: 'minute',
					count: 5,
					text: '5m'
				}, {
					type: 'minute',
					count: 20,
					text: '20m'
				}, {
					type: 'minute',
					count: 60,
					text: '1h'
				}, {
					type: 'minute',
					count: 180,
					text: '3h'
				}, {
					type: 'minute',
					count: 60*12,
					text: '12h'
				}, {
					type: 'day',
					count: 1,
					text: '1d'
				}, {
					type: 'week',
					count: 1,
					text: '1w'
				}, {
					type: 'month',
					count: 1,
					text: '1mo'
				}, {
					type: 'year',
					count: 1,
					text: '1y'
				}, {
					type: 'all',
					text: 'All'
				}]
			},
			
			series:[{
        id: "dataseries1",
				name: config.instrument,
				data: getFromData(data,1)
			},{
        id: "dataseries2",
				name: config.instrument + '2',
				data: getFromData(data,2)
			},
      getFlags(config.flags1, "dataseries1"),
      getFlags(config.flags2, "dataseries2")
      ]
		});
		series = chart.series[1]; // series[0] is YAxis series.
	};
	
  var preparedFlags = function(sim, update) {
    var flags = {1: [], 2: []};
    if (!update) 
      last_tr = 0;
    $.each (sim.data, function(i, row) {
      //does transaction go according to initial instrument
      var according = (row.instrument === sim.instrument);
      var nr = last_tr + 1;
      flags[according ? 1 : 2].push({x: localTime(row.timestamp), title: (nr)});
      tr_timestamp_max = Math.max(tr_timestamp_max, row.timestamp);
      last_tr = nr;
    });
    return flags;
  }
  
	var changeSeries = function(data) {
		chart.series[0].setData(getFromData(data,1), false);
		chart.series[1].setData(getFromData(data,2), false);
    chart.series[2].setData(config.flags1, false);
    chart.series[3].setData(config.flags2, false);
    chart.redraw();
    chart.hideLoading();
    
	};
	
  var dateFromTimestamp = function(t) {
    return new Date(t * 1000);
  }
  
  var startInterval = function() {
    fetchId = setInterval(function(){
      fetchNewData();
      Chart.showSimulation(activeSimulationId);
    }, 10000);
  }
  
  startInterval();
  
	return {
		init: function(c) {
      activeSimulationId = null;
			config = c;
			getData(redraw);
		},
		changeInstrument: function(inst) {
			config.instrument = inst;
			chart.showLoading('Loading');
			getData(changeSeries);
		},
    showSimulation: function(sim_id) {
      if (sim_id === null) //no simulation to display
        return;
      
      //do we only update simulation
      var update = (activeSimulationId === sim_id);
      activeSimulationId = sim_id;
      if (!update) {
        activeSimulationIsFinished = false;
        tr_timestamp_max = 0;
        last_tr = 0;
      }
      
      if (activeSimulationIsFinished) //no need to ask server, status is 100
        return;
      
      /* get transactions from server, only later than last update
       (later then 0 in case of !update) */
      var url = '/api/simulation/' + sim_id + "/" + tr_timestamp_max;
      
      $.ajax({
        url: url,
        dataType: 'JSON',
        success: function(sim) {
          if (sim.status === 100)
            activeSimulationIsFinished = true;

          var flags = preparedFlags(sim, update);
          if (update) { //only add new points
            for (k in flags[1]) {
              chart.series[2].addPoint(flags[1][k]);
            }
            for (k in flags[2]) {
              chart.series[3].addPoint(flags[2][k]);
            }
					} else { //change series
            config.flags1 = flags[1];
            config.flags2 = flags[2];
            Chart.changeInstrument(sim.instrument);
            chart.xAxis[0].setExtremes(
                    dateFromTimestamp(sim.from), 
                    dateFromTimestamp(sim.online ? timestamp_max : sim.to)
            );
          }
          sim['id'] = sim_id;
          config.callback(sim, update);
        }
      });
      
    },
		getPeriod: function() {
			return chart.xAxis[0].getExtremes();
		},
		redraw: function() {
			$(document).trigger('resize');
			chart.redraw();
		},
		toggleRealtime: function(status) {
			if(status && !fetchId) {
        startInterval();
        fetchNewData();
        Chart.showSimulation(activeSimulationId);
			}
			if(!status && fetchId) {
				clearInterval(fetchId);
				fetchId = null;
			}
		},
		getActiveSimulationId: function() {
			return activeSimulationId;
		}
	};
}();