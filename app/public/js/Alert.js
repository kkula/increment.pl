var Alert = function(config) {
	return {
		add: function(message,type,timeout) {
			if(type === undefined) {
				type = 'success';
			}
			
			var t = $.tmpl( $(config.tmpl).html() , {
				type: type,
				message: message
			} );
			console.log(t);
			
			var e = $(config.el).append(t).children().last().fadeIn();
			
			if(timeout) {
				setTimeout(function() {
					e.fadeOut(function() { $(this).remove(); });
				},timeout*1000);
			}
			
		},
		clear: function() {
			$(config.el).children().fadeOut(function() { $(this).remove(); });
		}
	};
}({
	el: '#alerts-container',
	tmpl: '#alert-element'
});