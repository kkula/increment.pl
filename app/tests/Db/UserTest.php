<?php

class Db_UserTest extends \PHPUnit_Framework_TestCase {
	
	
	public function testHashed() {
		
		// example how to test private methods.
		
		$method = new ReflectionMethod('\Increment\Db\User', 'hashed');
		$method->setAccessible(TRUE);
		
		$salt = \Increment\Db::getConfig('salt');
		
		$this->assertEquals(
				hash('sha256',$salt . 'qwerty'),
				$method->invoke(new \Increment\Db\User(),'qwerty'));
		
		$this->assertEquals(
				hash('sha256',$salt),
				$method->invoke(new \Increment\Db\User(),''));
		
		$this->assertEquals(
				hash('sha256',$salt . 'placki'),
				$method->invoke(new \Increment\Db\User(),'placki'));
    
  }
  
  public function testGetUserByMail() {
    $u = new \Increment\Db\User;
    $mail = 'piotrus.pan@gmail.com';
    $u->removeUserByMail($mail);
    $ret = $u->getUserByMail($mail);
    $this->assertEquals($ret, FALSE);
    
  }
  
  public function testCreateAccount() {
    
    $u = new \Increment\Db\User;
    $method = new ReflectionMethod('\Increment\Db\User', 'hashed');
		$method->setAccessible(TRUE);
    
    $mail = 'piotrus.pan@gmail.com';
    $name = 'Piotrus';
    $surname = 'Pan';
    $pass = 'passpass';
    $hashed_pass = $method->invoke($u, $pass);
    $ret = $u->createAccount($name, $surname, $mail, $pass);
    $this->assertEquals($ret, TRUE);
    $res = $u->getUserByMail($mail);
    $this->assertEquals($res['name'], $name);
    $this->assertEquals($res['surname'], $surname);
    $this->assertEquals($res['mail'], $mail);
    $this->assertEquals($res['password'], $hashed_pass);
    
    $ret = $u->createAccount($name, $surname, $mail, $pass);
    $this->assertEquals($ret, FALSE);
    
    $u->removeUserByMail($mail);
  }
  
  public function testChangePassword() {
    
    $u = new \Increment\Db\User;
    $method = new ReflectionMethod('\Increment\Db\User', 'hashed');
		$method->setAccessible(TRUE);
    
    $mail = 'piotrus.pan@gmail.com';
    $name = 'Piotrus';
    $surname = 'Pan';
    $pass = 'passpass';
    $new_pass = 'bassbass';
    $hashed_new_pass = $method->invoke($u, $new_pass);
    $hashed_pass = $method->invoke($u, $pass);
    $u->createAccount($name, $surname, $mail, $pass);
    $user = $u->getUserByMail($mail);
    $id = $user['id'];
    $ret = $u->changePassword($id, $pass+'x', $new_pass);
    $this->assertEquals($ret, FALSE);
    $ret = $u->changePassword($id, $pass, $new_pass);
    $this->assertEquals($ret, TRUE);
    $user = $u->getUserByMail($mail);
    $this->assertEquals($user['password'], $hashed_new_pass);
            
    $u->removeUserByMail($mail);
  }
  
  public function testCheckPassword() {
    
    $u = new \Increment\Db\User;
    
    $mail = 'piotrus.pan@gmail.com';
    $name = 'Piotrus';
    $surname = 'Pan';
    $pass = 'passpass';
    
    $u->createAccount($name, $surname, $mail, $pass);
    $user = $u->getUserByMail($mail);
    $id = $user['id'];
    $ret = $u->checkPassword($id, $pass+'x');
    $this->assertEquals($ret, FALSE);
    $ret = $u->checkPassword($id, $pass);
    $this->assertEquals($ret, TRUE);
            
    $u->removeUserByMail($mail);
  }
}