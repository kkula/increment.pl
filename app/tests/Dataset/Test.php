<?php

class DatasetTest extends \PHPUnit_Framework_TestCase {
	
	public function testGenral() {
		$c1 = new \Increment\Dataset\Constant(10);
		$this->assertEquals($c1->getValue(20) , 10);
	}
	
	public function testSum() {
		$c1 = new \Increment\Dataset\Constant(10);
		$c2 = new \Increment\Dataset\Constant(100);
		
		$this->assertEquals($c1->getValue(20) , 10);
		$this->assertEquals($c2->getValue(20) , 100);
		
		$s = new \Increment\Dataset\Sum($c1,$c2);
		$this->assertEquals($s->getValue(20) , 110);
		
		$s2 = new \Increment\Dataset\Sum($c1,$c2);
		$this->assertEquals($s2->getValue(20) , 110);
		
	}
	
	public function testDifference() {
		$c1 = new \Increment\Dataset\Constant(10);
		$c2 = new \Increment\Dataset\Constant(100);
		
		$this->assertEquals($c1->getValue(20) , 10);
		$this->assertEquals($c2->getValue(20) , 100);
		
		$s = new \Increment\Dataset\Difference($c1,$c2);
		$this->assertEquals($s->getValue(20) , -90);
		
		$s = new \Increment\Dataset\Difference($c2,$c1);
		$this->assertEquals($s->getValue(20) , 90);
	}
	
	public function testProduct() {
		$c1 = new \Increment\Dataset\Constant(10);
		
		$p = new \Increment\Dataset\Product($c1, $c1);
		
		$this->assertEquals($p->getValue(50), 10 * 10);
		
		// multiple.
		$p = new \Increment\Dataset\Product($p,$p);
		
		$this->assertEquals($p->getValue(100), 10000);
	}
	
	public function testCurrency() {
		// TODO: when test set will be prepared.
	}
	
	public function testExpressions() {
		
		// CONSTANT
		$c1 = new \Increment\Dataset\Constant(50);
		$this->assertEquals($c1->getExpression(), '50');
		
		// CURRENCY
		$c2 = new \Increment\Dataset\Currency('ALIPLNEUR');
		$this->assertEquals($c2->getExpression(),'CURRENCY(ALIPLNEUR)');
		
		// SUM
		$s = new \Increment\Dataset\Sum($c1,$c2);
		$this->assertEquals($s->getExpression(), '(50 + CURRENCY(ALIPLNEUR))');
		
	}
	
	public function testComplexOperations() {
		$c1 = new \Increment\Dataset\Constant(50);
		$c2 = new \Increment\Dataset\Constant(30);
		$c3 = new \Increment\Dataset\Constant(5);
		$c4 = new \Increment\Dataset\Constant(8);
		
		$x = new \Increment\Dataset\Product($c3, $c4);
		$y = new \Increment\Dataset\Difference($c2, $c1);
		
		// should be (30 - 50) + 5*8
		$s = new \Increment\Dataset\Sum($x, $y);
		$this->assertEquals($s->getValue(100), (30 - 50) + 5*8);
	}
}