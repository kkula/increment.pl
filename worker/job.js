$ = require('jQuery');


/**
 * Function fetching data from alior API and passing them as the only argument to callback.
 * Callback argument: array of objects containing name of pair, value and time when such value occured (as unix timestamp).
 * @param function callback
 */
var alior_function = function(callback) {
	var url = 'http://kantor.aliorbank.pl/forex/json/current';
	
	$.ajax({
		url: url,
		success: function(data) {
			
			var tm = parseInt((new Date(data.lastUpdate)).getTime() / 1000);
			
			var ret = [];
			
			for(var i in data.currencies) {
				var el = data.currencies[i];
				
				// TODO: filter currencies.
				var flag = false;
				for(i in instruments) {
					var x = instruments[i];
					var nm = el.currency1+el.currency2;
					if(x[0]+x[1] == nm || x[1]+x[0] == nm)
						flag = true;
				}
				if(!flag)
					continue;

				var buy = parseFloat(el.buy.replace(',','.'));
				var sell = parseFloat(el.sell.replace(',','.'));
				
				ret.push({
					name: el.currency1 + el.currency2,
					value: sell,
					timestamp: tm
				});
				
				ret.push({
					name: el.currency2 + el.currency1,
					value: (1.0/buy),
					timestamp: tm
				});
				
			}
			
			last_tm = tm;
			
			callback(ret);
			
		},
		error: function() {
			callback([]);
		}
	});
};



var truefx_function = function(callback) {

	//Documentation: http://www.truefx.com/dev/data/TrueFX_MarketDataWebAPI_DeveloperGuide.pdf
	
	//Check whether we're authorized:
	if ( typeof this.auth === 'undefined' ) {
		$.ajax({
			url: 'http://webrates.truefx.com/rates/connect.html?u=increment&p=passpass&q=incrementpl&c=CHF/JPY,EUR/CHF,EUR/GBP,EUR/JPY,EUR/USD,GBP/JPY,GBP/USD,USD/CHF,USD/JPY&f=csv&s=n',
			success: function(data) {
				if (data == 'not authorized')
				{
					console.log('TrueFX authorization failed');
					callback([]);
				}
				else
				{	
					console.log('TrueFX authorized');
					this.auth = data;
				}
			},
		 	error: function() {
				callback([]);
			}
		});
		
		callback([]);
	}
	console.log('this.auth = '+this.auth);
	
	var url = 'http://webrates.truefx.com/rates/connect.html?id='+this.auth;
	
	$.ajax({
		url: url,
		success: function(data) {
			var ret = [];
			var tm;
			
			for(var i in data.split(' ')) {
				
				for(var j in data.split(',')) {
					var currency1 = j[0].split('/')[0];
					var currency2 = j[0].split('/')[1];
								
					tm = Math.max(tm, parseInt(j[1]));
					
					var sell = parseFloat(j[2])+parseFloat(j[3])/100000;
					var buy = parseFloat(j[4])+parseFloat(j[5])/100000;
					
					ret.push({
						name: currency1 + currency2,
						value: sell,
						timestamp: tm
					});
					
					ret.push({
						name: currency2 + currency1,
						value: (1.0/buy),
						timestamp: tm
					});
				}
			}
			
			last_tm = tm;
			
			callback(ret);
			
		},
		error: function() {
			callback([]);
		}
	});
};


var exec = require('child_process').exec;
var last_tm = null;

var sources = {
	ALI: alior_function,
	TRU: truefx_function
};

var grabber_add = function(cantor_name, ret) {
	for(var i in ret) {
		var x = ret[i];
		var q = client.query('INSERT INTO rates(instrument_id,timestamp,value) VALUES($1,$2,$3)',[cantor_name+x.name, x.timestamp, x.value]);
		q.on('error',function() { /* empty handler */});
	}
};

var grabber_fetch = function(name) {
		console.log( (new Date()).getTime() +': Fetching');
		
		sources[name](function(ret) {
			grabber_add(name, ret);
			
			// calling online simulator
			var child = exec('php ../app/cli.php o ' + last_tm,
			function(err,stdout,stderr) {
				// done
				console.log(stdout);
			}
			);
			
			
			setTimeout(function() { grabber_fetch(name); },5000);
		});
};


var grabber = function() {
	for(var name in sources) {
			grabber_fetch(name);
	}
};


// loading config
require('js-yaml');
var doc = require('../app/config/config.production.yml');
console.log(doc)
var instruments = doc.instruments;

var db = doc.config.db;

// database configuration
var pg_connect = 'postgres://' + db.username + ':' + db.password + '@' + db.host + ':5432/' + db.dbname;
console.log(pg_connect);
var pg = require('pg');
var client = new pg.Client(pg_connect);
client.connect();


grabber();
