$ = require('jQuery');
var mdb = require('mongodb');

var server = new mdb.Server("127.0.0.1",27017,{});

function fn(c) {
	$.ajax({
url: 'http://kantor.aliorbank.pl/forex/json/current',
	success: function(data) {
		c.insert(data,function(err,docs) {
			setTimeout(function() { fn(c); },5000);
		});
	},
	error: function() {
		setTimeout(function() { fn(c); },5000);
	}
	});
};

new mdb.Db('iolab',server,{}).open(function(err,client) {
	if(err) throw err;
	var collection = new mdb.Collection(client,'alior');
	client.ensureIndex('alior',{lastUpdate: 1},{unique: true});
	fn(collection);
});
